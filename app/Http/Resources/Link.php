<?php

namespace App\Http\Resources;

use App\Http\Controllers\LinkController;
use Illuminate\Http\Resources\Json\JsonResource;

class Link extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            'shortlynk' => LinkController::$base . $this->shortlynk,
            'redirect' => $this->redirect,
            'created_at' => $this->created_at,
        ];
    }
}
