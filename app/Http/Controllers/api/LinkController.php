<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\Link as LinkResource;
use App\Models\Link;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;


class LinkController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api');
    }

    public function index()
    {
        return LinkResource::collection(Link::all());
    }

    public function show(Link $link, Request $request)
    {
        if (auth()->guard('api')->user()->id !== $link->user_id)
            return response()->json(['error' => 'Unauthorised Action'], 401);

        return new LinkResource($link);
    }

    public function store(Request $request)
    {
        try{
            $request->validate([
                'redirect' => 'required',
            ]);

            do {
                $shortlynk = Str::random(7);
            } while( Link::where('shortlynk',$shortlynk )->count() > 0);

            $link = Link::create( array(
                    'shortlynk' => $shortlynk,
                    'redirect' => $request->input('redirect'),
                    'user_id' => auth()->guard('api')->user()->id
                )
            );

            if ($link->save())
                return new LinkResource($link);
            else
                return response()->json(['error' => 'Link Could not be added'], 401);

        } catch (\Exception $e){
            return response()->json(['error' => $e->errors()], 401);
        }

    }

    public function update(Link $link,Request $request)
    {
        if (auth()->guard('api')->user()->id !== $link->user_id)
            return response()->json(['error' => 'Unauthorised Action'], 401);

        $link->shortlynk = $request->input('shortlynk') ? : $link->shortlynk;

        if ($link->save()){
            $link->shortlynk = \App\Http\Controllers\LinkController::$base . $link->shortlynk ;
            return new LinkResource($link);
        }
        else
            return response()->json(['error' => 'Book Could not be updated'], 401);
    }

    public function destroy(Link $link)
    {
        if (auth()->guard('api')->user()->id !== $link->user_id)
            return response()->json(['error' => 'Unauthorised Action'], 401);

        if ($link->delete())
            return new LinkResource($link);
    }
}
