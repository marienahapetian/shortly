<?php

namespace App\Http\Controllers;

use App\Models\Click;
use App\Models\Link;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class LinkController extends Controller
{
    public static $base = 'short.ly/';

    public function show( $id )
    {
        $link = Link::findOrFail($id);
        return view('link.single',compact('link'));
    }

    public function create( Request $request )
    {
        if(!Auth::check()) return response()->json(
            ['error' => 'You need to log in first'],
            '401'
        );
        $validation = Validator::make( Input::all(),[
            'url' => 'required|url'
        ]);

        if( $validation->fails() ) {
            return response()->json(
                ['error' => 'Invalid URL format']
            );
        } else {
            $link = Link::where('redirect',Input::get('url')) -> first();

            if( $link ) {
                return response()->json(
                    ['error' => 'You already have a shortlynk related to that URL']
                );

            } else {
                do {
                    $shortlynk = Str::random(7);
                } while( Link::where('shortlynk',$shortlynk )->count() > 0);

                $link = Link::create( array(
                    'shortlynk' => $shortlynk,
                    'redirect' => Input::get('url'),
                    'user_id' => Auth::user()->id
                    )
                );

                return response()->json(
                    [
                        'success' => true,
                        'shortlynk' => static::$base . $link->shortlynk,
                        'redirect_url' => $link->redirect
                    ]
                );
            }
        }
    }

    public function edit( Request $request )
    {
        $validation = Validator::make( Input::all(),[
            'shortlynk' => 'required'
        ]);

        if( $validation->fails() ) {
            return response()->json(
                [
                    'error' => 'Shortlynk is required',
                ]
            );
        } else {
            $id = $request->id;
            $link = Link::find($id);
            if($link){
                $new_link = Link::where('shortlynk',$request->shortlynk)->first();
                if($new_link){
                    return response()->json(
                        [
                            'error' => 'Such shortlynk already exists',
                        ]
                    );
                } else {
                    $link->shortlynk = $request->shortlynk;
                    $link->save();
                    return response()->json(
                        [
                            'success' => true,
                            'shortlynk' => static::$base . $link->shortlynk
                        ]
                    );
                }

            } else {
                return response()->json(
                    [
                        'error' => 'Shortlynk does not exist',
                    ]
                );
            }
        }

    }

    public function delete( $link )
    {

    }

    public function redirect( Request $request, $shortlink )
    {
        $link = Link::where('shortlynk',$shortlink)->firstOrFail();

        Click::create( array(
            'link_id' => $link->id, 'created_at' => Carbon::now(), 'ip_address' => $request->ip())
        );

        return redirect($link->redirect);
    }
}
