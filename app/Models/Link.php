<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = ['shortlynk','redirect','user_id'];


    public function clicks()
    {
        return $this->hasMany('\App\Models\Click');
    }
}
