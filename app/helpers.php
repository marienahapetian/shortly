<?php
function get_video($url){
    if (strpos($url, 'youtube') > 0) {
        $youtube = "http://www.youtube.com/oembed?url=". $url ."&format=json";

        $curl = curl_init($youtube);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($curl);
        curl_close($curl);
        return json_decode($return, true);
    } elseif(strpos($url, 'vimeo') > 0){
        return get_vimeo_info(get_vimeo_id($url));
    } else{

    }
}

function get_vimeo_id($url) {
    $regexstr = '#(http|https)://(?:\w+.)?vimeo.com/(?:video/|moogaloop\.swf\?clip_id=)*(\w+)#i';
    preg_match($regexstr,$url, $matches);
    return $matches[2];
}

function get_vimeo_info($id) {
    if (!function_exists('curl_init')):
        die('CURL is not installed!');
    else:
        $cr = curl_init();
        curl_setopt($cr, CURLOPT_URL, "https://vimeo.com/api/v2/video/$id.php"); // the vimeo simple api with php output
        curl_setopt($cr, CURLOPT_HEADER, 0);
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cr, CURLOPT_TIMEOUT, 10);
        $output = unserialize(curl_exec($cr));
        $output = $output[0];
        curl_close($cr);
        return $output;
    endif;
}

function get_iframe($url)
{
    $info = get_video($url);

    if(isset($info['html'])){
        return $info['html'];
    } elseif(isset($info['thumbnail_large'])) {
        return '<img src="'.$info['thumbnail_large'].'"/>';
    } else {
        return '<img src="'.asset('/img/S.png').'"/>';
    }
}

function get_thumbnail($url)
{
    $info = get_video($url);

    if(isset($info['thumbnail_url'])){
        return '<img src="'.$info['thumbnail_url'].'"/>';
    } elseif(isset($info['thumbnail_large'])) {
        return '<img src="'.$info['thumbnail_large'].'"/>';
    } else {
        return '<img src="'.asset('/img/S.png').'"/>';
    }
}

function get_video_title($url)
{
    $info = get_video($url);

    if(isset($info['title'])){
        return '<i class="fas fa-info"></i>' . $info['title'];
    } else {
        return '<i class="fas fa-info"></i> No Title';
    }
}