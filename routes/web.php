<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/client',function (){
    return view('client');
})->name('client');

Route::get('/documentation', function () {
    return view('documentation');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/create', 'LinkController@create')->name('create');

Route::put('/edit', 'LinkController@edit')->name('edit');

Route::get('/link/{id}', 'LinkController@show')->name('single');

Route::get('/{shortlink}','LinkController@redirect')->name('redirect');
