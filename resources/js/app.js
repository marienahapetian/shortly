
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

const app = new Vue({
    el: '#app'
});

$(document).ready(function () {
    function getRedirectSiteTitle(url){
        $.ajax({
            url: url,
            success: function(data) {
                console.log(data);
                alert(data.responseText);
            }
        });
    }

    $(document).on('click','.modal-footer button.btn-success',function(){
        var form = $(this).closest('.modal-content').find('form');
        var method = form.attr('method');
        var url = form.attr('action');
        var formData = form.serialize();
        var redirect_url;

        $.ajax({
            type: method,
            url: url,
            data: formData,
            success:function(data){
                if(data.success){
                    form.append('<span class="success">Your ShortLynk '+ data.shortlynk +'</span>');
                }

                setTimeout(function () {
                    location.reload();
                },3000)
            }
        });
    });


    $(document).on('click','.modal-toggle',function(e){
        e.preventDefault();
        var shortlynk = $(this).attr('data-shortlynk');
        var id = $(this).attr('data-id');
        $('input[name=shortlynk]').val(shortlynk);
        $('input[name=id]').val(id);

        $(this).next('button').trigger('click');
    })
})