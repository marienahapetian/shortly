@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 " style="background: #fff">
                    <p class="title row">{{ \App\Http\Controllers\LinkController::$base . $link->shortlynk}}</p>

                    <div class="row">
                        <div class="col-md-6">
                            <p class="thumb text-center">
                                {!!get_iframe($link->redirect)!!}
                            </p>
                        </div>

                        <div class="col-md-6">
                            <p class="info">{!! get_video_title($link->redirect) !!}</p>

                            <p class="link"><i class="fas fa-link"></i>{{$link->redirect}}</p>

                            <h2>{{__('Clicks')}}</h2>
                            @if(count($link->clicks))
                                @foreach($link->clicks as $click)
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p>{{__('IP Address: ')}} {{$click->ip_address}}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p>{{$click->created_at}}</p>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class=""><i class="far fa-frown"></i>{{__('Noone clicked on your link yet')}}</div>
                            @endif
                        </div>
                    </div>


            </div>
        </div>
    </div>
@endsection