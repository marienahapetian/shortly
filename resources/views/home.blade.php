@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="row text-center">

                            <div class="col-md-4">
                                <label>{{__('Total Clicks')}}</label>
                                <span class="digital-result">
                                    {{ count(\Illuminate\Support\Facades\Auth::user()->clicks?:'0') }}
                                </span>
                            </div>

                            <div class="col-md-4">
                                <label>{{__('ShortLynks')}}</label>
                                <span class="digital-result">
                                    {{ count(\Illuminate\Support\Facades\Auth::user()->shortlynks?:'0') }}
                                </span>
                            </div>

                            <div class="col-md-4">
                                <label>{{__('Last Click')}}</label>
                                <span class="digital-result-sm">
                                    @if(\Illuminate\Support\Facades\Auth::user()->clicks->last())
                                        {{ \Illuminate\Support\Facades\Auth::user()->clicks->last()->created_at?:'' }}
                                        {{ \Illuminate\Support\Facades\Auth::user()->clicks->last()->ip_address?:'' }}
                                    @else
                                        {{__('No Clicks Yet')}}
                                    @endif
                                </span>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="list-group" style="margin-top: 30px">
                    @foreach(\Illuminate\Support\Facades\Auth::user()->shortlynks as $shortlynk)
                        <div class="list-group-item shortlynk-item">
                            <div class="col-md-2">
                                {!!get_thumbnail($shortlynk->redirect)!!}
                            </div>
                            <div class="col-md-5 link">
                                {{$shortlynk->redirect}}
                            </div>
                            <div class="col-md-2 shortlynk">
                                <a href="{{route('single',$shortlynk->id)}}">{{$base_url . $shortlynk->shortlynk}}</a>
                            </div>
                            <div class="col-md-2 actions text-center">
                                <a href="" class="modal-toggle" data-shortlynk="{{$shortlynk->shortlynk}}" data-id="{{$shortlynk->id}}">{{__('Edit')}}</a>
                                <button type="button" class="hidden btn btn-primary" data-toggle="modal" data-target="#shortlynk-edit">
                                    {{__('Edit')}}
                                </button>
                            </div>
                            <div class="col-md-2 shortlynk">
                                {{$shortlynk->created_at}}
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="modal fade" id="shortlynk-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">{{__('Edit ShortLynk')}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="edit" action="{{route('edit')}}" method="PUT">
                                    @csrf
                                    <input type="text" name="shortlynk" value="" >
                                    <input type="hidden" name="id" value="" >
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                                <button type="button" class="btn btn-success">{{__('Shortlynk Me')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
